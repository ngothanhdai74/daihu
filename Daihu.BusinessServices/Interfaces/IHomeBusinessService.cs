﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.BusinessServices.Interfaces
{
    public interface IHomeBusinessService
    {
        Task Call();
    }
}
