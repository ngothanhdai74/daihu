﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Daihu.BusinessServices.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.BusinessServices.Implements
{
    public class HomeBusinessService : IHomeBusinessService
    {
        private readonly IConfiguration _configuration;

        public HomeBusinessService( IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task Call()
        {
            var aswSection = _configuration.GetSection("Aws");
            var accessKey = aswSection.GetSection("AccessKey").Value;
            var secretKey = aswSection.GetSection("SecretKey").Value;
            var sqsUrl = aswSection.GetSection("SQSUrl").Value;

            var credentials = new Amazon.Runtime.BasicAWSCredentials(accessKey, secretKey);
            
            AmazonSQSClient amazonSQSClient = new AmazonSQSClient(credentials, Amazon.RegionEndpoint.USWest2);

            SendMessageRequest sendMessageRequest = new SendMessageRequest();
            sendMessageRequest.QueueUrl = sqsUrl;
            sendMessageRequest.MessageBody = "đại dz";
            SendMessageResponse sendMessageResponse = await amazonSQSClient.SendMessageAsync(sendMessageRequest);


        }
    }
}
