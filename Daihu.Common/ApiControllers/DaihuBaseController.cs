﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace Daihu.Common.DaihuController
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class DaihuBaseController : ControllerBase
    {

    }
}
