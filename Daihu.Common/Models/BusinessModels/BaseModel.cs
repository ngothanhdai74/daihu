﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Daihu
{
    public class BaseModel
    {
        public long Id { get; set; }
        public int Status { get; set; }
        public DateTime? CreateDateUtc { get; set; }
        public DateTime? UpdateDateUtc { get; set; }
        public string CreateUid { get; set; }
        public string UpdateUid { get; set; }
    }
}
