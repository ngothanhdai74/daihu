﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Daihu.Common.Models
{
    public class BaseWriteModel
    {
        public long Id { get; set; }
        public int Status { get; set; }
        public DateTime? CreateDateUtc { get; set; }
        public string CreateUid { get; set; }
        public DateTime? UpdateDateUtc { get; set; }
        public string UpdateUid { get; set; }
    }
}
