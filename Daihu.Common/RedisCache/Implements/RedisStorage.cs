﻿using Daihu.Caching.Interfaces;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.Caching.Implements
{
    public class RedisStorage : IRedisStorage
    {
        private readonly IDatabase _writeDatabase;
        private readonly IDatabase _readDatabase;
        public RedisStorage(int dbId = 1)
        {
            _writeDatabase = RedisConnection.GetCurrentWriteConnection(dbId);
            _readDatabase = RedisConnection.GetCurrentReadConnection(dbId);
        }
        public async Task<bool> StringSet(string key, object value, TimeSpan? timeout)
        {
            RedisValue redisValue = Serialization.ProtoBufSerialize(value);
            return await _writeDatabase.StringSetAsync(key, redisValue, timeout.HasValue ? timeout.Value : TimeSpan.FromDays(10));
        }
        public async Task<T> StringGet<T>(string key)
        {
            RedisValue value = await _readDatabase.StringGetAsync(key);
            return Serialization.ProtoBufDeserialize<T>(value);
        }
        public async Task<bool> KeyDelete(string key) => await _writeDatabase.KeyDeleteAsync(key);
        public async Task<bool> HashSet(string key, string field, object value)
        {
            RedisValue redisValue = Serialization.ProtoBufSerialize(value);
            return await _writeDatabase.HashSetAsync(key, field, redisValue);
        }
        public async Task<T> HashGet<T>(string key, string field)
        {
            RedisValue redisValue = await _readDatabase.HashGetAsync(key, field);
            return Serialization.ProtoBufDeserialize<T>(redisValue);
        }
        public async Task<bool> HashDelete(string key, string field) => await _writeDatabase.HashDeleteAsync(key, field);
    }
}
