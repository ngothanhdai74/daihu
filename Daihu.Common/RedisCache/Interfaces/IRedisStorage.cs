﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.Caching.Interfaces
{
    public interface IRedisStorage
    {
        Task<bool> StringSet(string key, object value, TimeSpan? timeout);
        Task<T> StringGet<T>(string key);
        Task<bool> KeyDelete(string key);
        Task<bool> HashSet(string key, string field, object value);
        Task<T> HashGet<T>(string key, string field);
        Task<bool> HashDelete(string key, string field);
    }
}
