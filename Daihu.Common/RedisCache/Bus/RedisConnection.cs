﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.Caching
{
    public class RedisConnection
    {
        public static readonly string RedisPass = "Davidkmhd!1";
        public static readonly string ServerIp = "34.121.97.67:6379";
        private const int SyncTimeout = 100000;
        private static volatile RedisConnection _instance;
        public static readonly object SyncLock = new object();
        public static readonly object SyncConnectionLock = new object();
        public static readonly object SyncReadConnectionLock = new object();
        private static SocketManager _socketManager;
        private ConnectionMultiplexer _writeConnection;
        private ConnectionMultiplexer[] _readConnections;
        public static int ReadIndexConnection = 0;
        public static string[] ServerIps => ServerIp.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        public async Task<List<string>> GetServer(bool isMaster)
        {
            var config = ConfigurationOptions.Parse(ServerIp);
            config.SyncTimeout = SyncTimeout;
            config.AbortOnConnectFail = false;
            config.AllowAdmin = true;
            config.SocketManager = _socketManager;
            config.Password = RedisPass;
            var connection = await ConnectionMultiplexer.ConnectAsync(config);
            List<string> endPoints = new List<string>();
            foreach (var endPoint in ServerIps)
            {
                var server = connection.GetServer(endPoint);
                if (isMaster)
                {
                    if (server != null && server.IsConnected && !server.IsReplica) return new List<string>() { endPoint };
                }
                else
                {
                    if (server != null && server.IsConnected)
                    {
                        endPoints.Add(endPoint);
                    }
                }
            }
            return endPoints;
        }
        public static RedisConnection Current
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new RedisConnection();
                        }
                    }
                }
                return _instance;
            }
        }
        private RedisConnection()
        {
            _socketManager = new SocketManager(GetType().Name);
            _writeConnection = GetNewWriteConnection().Result;
            _readConnections = GetNewReadConnections().Result;
        }
        private async Task<ConnectionMultiplexer> GetNewWriteConnection()
        {
            var writeIp = await GetServer(true);
            if (writeIp?.Any() == true)
            {
                var config = ConfigurationOptions.Parse(writeIp.First());
                config.KeepAlive = 180;
                config.SyncTimeout = SyncTimeout;
                config.AbortOnConnectFail = true;
                config.AllowAdmin = true;
                config.SocketManager = _socketManager;
                config.ConnectRetry = 5;
                config.Password = RedisPass;
                return await ConnectionMultiplexer.ConnectAsync(config);
            }
            throw new Exception("Redis Write not found");
        }
        private async Task<ConnectionMultiplexer[]> GetNewReadConnections()
        {
            var readIps = await GetServer(false);
            if (readIps?.Any() == true)
            {
                ConnectionMultiplexer[] connectionMultiplexers = new ConnectionMultiplexer[readIps.Count];
                for (int i = 0; i < readIps.Count; i++)
                {
                    var config = ConfigurationOptions.Parse(readIps[i]);
                    config.KeepAlive = 180;
                    config.SyncTimeout = SyncTimeout;
                    config.AbortOnConnectFail = true;
                    config.AllowAdmin = true;
                    config.SocketManager = _socketManager;
                    config.ConnectRetry = 5;
                    config.Password = RedisPass;
                    connectionMultiplexers[i] = ConnectionMultiplexer.Connect(config);
                }
            }
            throw new Exception("Redis Read not found");
        }
        public ConnectionMultiplexer GetWriteConnection
        {
            get
            {
                lock (SyncConnectionLock)
                {
                    if (_writeConnection == null)
                    {
                        _writeConnection = GetNewWriteConnection().Result;
                    }
                    else
                    {
                        if (!_writeConnection.IsConnected) _writeConnection = GetNewWriteConnection().Result;
                    }
                    return _writeConnection;
                }
            }
        }
        public static IDatabase GetCurrentWriteConnection(int dbId) => Current.GetWriteConnection.GetDatabase(dbId);
        public ConnectionMultiplexer GetReadConnection
        {
            get
            {
                lock (SyncReadConnectionLock)
                {
                    var connection = _readConnections[ReadIndexConnection++];
                    if (ReadIndexConnection >= _readConnections.Length)
                    {
                        ReadIndexConnection = 0;
                    }

                    if (connection == null)
                    {
                        _readConnections = GetNewReadConnections().Result;
                    }
                    else
                    {
                        if (!connection.IsConnected)
                            _readConnections = GetNewReadConnections().Result;
                    }
                    return connection;
                }
            }
        }
        public static IDatabase GetCurrentReadConnection(int dbId) => Current.GetReadConnection.GetDatabase(dbId);
    }
}
