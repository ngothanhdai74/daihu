﻿using AutoMapper;
using Daihu.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Daihu
{
    public class BaseBusinessService
    {
        private readonly IMapper _mapper;
        public BaseBusinessService(IMapper mapper)
        {
            _mapper = mapper;
        }
        public TDestication ToModel<TSource, TDestication>(TSource source)
            where TDestication : BaseModel where TSource : BaseReadModel => _mapper.Map<TDestication>(source);
        public TDestication ToWModel<TSource, TDestication>(TSource source)
            where TDestication : BaseWriteModel where TSource : BaseModel
        { 
            var model = _mapper.Map<TDestication>(source);
            model.CreateDateUtc = model.CreateDateUtc ?? DateTime.UtcNow;
            model.Status = model.Status == 0 ? 1 : model.Status;
            model.UpdateDateUtc = model.UpdateDateUtc ?? DateTime.UtcNow;
            return model;
        } 
        public IList<TDestication> ToModels<TSource, TDestication>(IList<TSource> sources)
            where TDestication : BaseModel where TSource : BaseReadModel => sources.Select(m => ToModel<TSource, TDestication>(m)).ToList();
        public IList<TDestication> ToWModels<TSource, TDestication>(IList<TSource> sources)
            where TDestication : BaseWriteModel where TSource : BaseModel => sources.Select(m => ToWModel<TSource, TDestication>(m)).ToList();
    }
}
