﻿using AutoMapper;
using Daihu.Common.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace Daihu
{
    public abstract class BaseRepository<TDbContext, TModel, RModel, WModel> : IBaseRepository<TModel, RModel, WModel>
        where TDbContext : DbContext
        where TModel : class
        where RModel : BaseReadModel
        where WModel : BaseWriteModel
    {
        protected readonly TDbContext _dbContext;
        private readonly IMapper _mapper;
        public BaseRepository(IMapper mapper, TDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }
        #region Mapping
        private RModel ToModel(TModel model) => _mapper.Map<RModel>(model);
        private TModel ToModel(WModel model) => _mapper.Map<TModel>(model);
        private IList<RModel> ToModels(IQueryable<TModel> models) => models.Select(m => ToModel(m)).ToList();
        private IList<TModel> ToModels(IList<WModel> models) => models.Select(m => ToModel(m)).ToList();
        #endregion
        #region Read
        public virtual async Task<IList<RModel>> QueryAsync(Paging paging, params Expression<Func<TModel, bool>>[] expressions)
        {
            var data = _dbContext.Set<TModel>().AsQueryable();
            foreach (Expression<Func<TModel, bool>> expression in expressions) data = data.Where(expression);
            if (paging != null)
            {
                paging.TotalRow = await data.CountAsync();
                data = data.Skip(paging.PageIndex * paging.PageSize).Take(paging.PageSize);
            }
            return ToModels(data);
        }
        public virtual async Task<RModel> FindAsync(Expression<Func<TModel, bool>> expression) => ToModel(await _dbContext.Set<TModel>().FirstOrDefaultAsync(expression));
        public virtual async Task<RModel> GetByIdAsync(long id) => ToModel(await _dbContext.Set<TModel>().FindAsync(id));
        #endregion
        #region Write
        public virtual async Task<long> AddAsync(WModel t)
        {
            try
            {
                var model = ToModel(t);
                await _dbContext.Set<TModel>().AddAsync(model);
                await _dbContext.SaveChangesAsync();
                return t.Id;
            }
            catch
            {
                return -1;
            }
        }
        public virtual async Task<long> UpdateAsync(WModel t)
        {
            try
            {
                var model = ToModel(t);
                _dbContext.Set<TModel>().Update(model);
                await _dbContext.SaveChangesAsync();
                return t.Id;
            }
            catch
            {
                return -1;
            }
        }
        public virtual async Task<long> DeleteAsync(long t)
        {
            try
            {
                var result = await _dbContext.Set<TModel>().FindAsync(t);
                if (result != null)
                {
                    _dbContext.Set<TModel>().Remove(result);
                    await _dbContext.SaveChangesAsync();
                    return t;
                }
            }
            catch { }
            return -1;
        }
        #endregion
    }
}
