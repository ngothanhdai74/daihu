﻿using Daihu.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Daihu
{
    public interface IBaseRepository<TModel, RModel, WModel>
        where TModel : class
        where RModel : BaseReadModel
        where WModel : BaseWriteModel
    {
        Task<long> DeleteAsync(long t);
        Task<long> UpdateAsync(WModel t);
        Task<long> AddAsync(WModel t);
        Task<RModel> GetByIdAsync(long id);
        Task<RModel> FindAsync(Expression<Func<TModel, bool>> expression);
        Task<IList<RModel>> QueryAsync(Paging paging, params Expression<Func<TModel, bool>>[] expressions);
    }
}
