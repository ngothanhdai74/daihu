﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using IdentityModel.Client;
using System.Net.Http.Json;
namespace Daihu.Common.FeServices
{
    public class BaseFeService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        public BaseFeService(IHttpClientFactory clientFactory, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _clientFactory = clientFactory;
        }
        protected async Task<TResponse> Call<TResponse, TRequest>(string url, HttpMethod method, TRequest model)
        {
            var client = _clientFactory.CreateClient(_configuration["HttpClientName"]);
            client.SetBearerToken(await _httpContextAccessor.HttpContext.GetTokenAsync("access_token"));
            var request = new HttpRequestMessage(method, url)
            {
                Content = JsonContent.Create(model)
            };
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<TResponse>();
        }
    }
}
