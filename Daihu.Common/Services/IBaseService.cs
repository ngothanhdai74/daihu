﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Daihu
{
    public interface IBaseService<TModel, RModel, WModel>
    {
        Task<long> DeleteAsync(long t);
        Task<long> UpdateAsync(WModel t);
        Task<long> AddAsync(WModel t);
        Task<RModel> GetByIdAsync(long id);
        Task<RModel> FindAsync(Expression<Func<TModel, bool>> expression);
        Task<IList<RModel>> QueryAsync(Paging paging, params Expression<Func<TModel, bool>>[] expressions);
    }
}
