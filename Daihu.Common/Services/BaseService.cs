﻿using Daihu.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Daihu
{
    public class BaseService<TModel, RModel, WModel> : IBaseService<TModel, RModel, WModel>
        where TModel : class
        where RModel : BaseReadModel
        where WModel : BaseWriteModel
    {
        protected readonly IBaseRepository<TModel,RModel,WModel> _baseRepository;
        public BaseService(IBaseRepository<TModel, RModel, WModel> baseRepository)
        {
            _baseRepository = baseRepository;
        }
        public virtual async Task<long> UpdateAsync(WModel t) => await _baseRepository.UpdateAsync(t);
        public virtual async Task<long> AddAsync(WModel t) => await _baseRepository.AddAsync(t);
        public virtual async Task<long> DeleteAsync(long t) => await _baseRepository.DeleteAsync(t);
        public virtual async Task<RModel> FindAsync(Expression<Func<TModel, bool>> expression) => await _baseRepository.FindAsync(expression);
        public virtual async Task<RModel> GetByIdAsync(long id) => await _baseRepository.GetByIdAsync(id);
        public virtual async Task<IList<RModel>> QueryAsync(Paging paging, params Expression<Func<TModel, bool>>[] expressions) => await _baseRepository.QueryAsync(paging, expressions);
    }
}
