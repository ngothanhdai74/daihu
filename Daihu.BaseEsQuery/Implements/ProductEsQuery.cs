﻿using Daihu.BaseEsQuery.Interfaces;
using Daihu.BaseEsQuery.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.BaseEsQuery.Implements
{
    public class ProductEsQuery: IProductEsQuery
    {
        private readonly ElasticClient _client;
        public ProductEsQuery()
        {
            string url = "34.121.97.67:9200";
            var settings = new ConnectionSettings(new Uri(url));
            _client = new ElasticClient(settings);
        }
        public async Task<bool> CreateIndex()
        {
            var index = await _client.Indices.CreateAsync("dai", c => c
                .Settings(set => set
                    .Setting(UpdatableIndexSettings.MaxNGramDiff, 32)
                    .Analysis(a => a
                        .TokenFilters(token => token
                            .EdgeNGram("edgeNGram_filter", eg => eg
                                 .MinGram(2)
                                 .MaxGram(10)
                            )
                            .AsciiFolding("my_ascii_folding", asc => asc
                                 .PreserveOriginal(true)
                            )
                        )
                        .Analyzers(ana => ana
                            .Custom("index_question", cus => cus
                                 .Tokenizer("standard")
                                 .Filters("lowercase", "edgeNGram_filter", "my_ascii_folding")
                            )
                            .Custom("search_question", cus => cus
                                 .Tokenizer("standard")
                                 .Filters("lowercase", "my_ascii_folding")
                            )
                        )
                    )
                )
                .Map<ProductEs>(ma => ma
                    .AutoMap()
                    .Properties(pro => pro
                        .Text(t => t
                            .Name(na => na.Name)
                            .Analyzer("index_question")
                            .SearchAnalyzer("search_question")
                        )
                        .Text(tt => tt
                            .Name(na => na.Description)
                            .Analyzer("index_question")
                            .SearchAnalyzer("search_question")
                        )
                    )
                )
            );
            return index.IsValid;
        }
    }
}
