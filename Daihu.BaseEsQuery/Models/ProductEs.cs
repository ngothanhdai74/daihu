﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.BaseEsQuery.Models
{
    public class ProductEs
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public long? CategoryId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
