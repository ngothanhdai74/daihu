﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class VsConfigDetailRepository : BaseRepository
    {
        public VsConfigDetailRepository()
        {

        }
        private DataTable GetConfigDetailByConfigIdDataTable(string configId) => Gets(DaihuSqlCommand.GetConfigDetailByConfigId(configId));
        public List<ConfigDetailModel> GetConfigDetailByConfigId(string configIdParam)
        {
            List<ConfigDetailModel> models = new List<ConfigDetailModel>();
            var data = GetConfigDetailByConfigIdDataTable(configIdParam);
            if (data?.Rows.Count > 0)
            {
                foreach (DataRow row in data?.Rows)
                {
                    var id = row[0];
                    var configId = row[1];
                    var status = row[2];
                    var fileName = row[3];
                    var folderPath = row[4];
                    var textContent = row[5];
                    var createdUid = row[6];
                    var createdDateUtc = row[7];
                    var updatedDateUtc = row[8];
                    var updatedUid = row[9];
                    models.Add(new ConfigDetailModel()
                    {
                        Id = (string)id,
                        ConfigId = (string)configId,
                        Status = (short)status,
                        FileName = (string)fileName,
                        FolderPath = (string)folderPath,
                        TextContent = (string)textContent,
                        CreatedUid = (string)createdUid,
                        CreatedDateUtc = (DateTime?)createdDateUtc,
                        UpdatedDateUtc = (DateTime?)updatedDateUtc,
                        UpdatedUid = (string)updatedUid
                    });
                }
            }
            return models;
        }
        public bool Delete(string configId)
        {
            var queryString = DaihuSqlCommand.DeleteConfigDetailByConfigId(configId);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        public bool DeleteById(string id)
        {
            var queryString = DaihuSqlCommand.DeleteConfigDetail(id);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        public bool Insert(ConfigDetailModel model)
        {
            var queryString = DaihuSqlCommand.InsertConfigDetail(model);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        public bool Update(ConfigDetailModel model)
        {
            var queryString = DaihuSqlCommand.UpdateConfigDetail(model);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
    }
}
