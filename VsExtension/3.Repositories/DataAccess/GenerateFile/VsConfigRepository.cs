﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace VsExtension.Repositories.DataAccess
{
    public class VsConfigRepository : BaseRepository
    {
        public VsConfigRepository() { }
        private DataTable GetAllConfigDataTable() => Gets(DaihuSqlCommand.GetAllConfig());
        public List<ConfigModel> GetAllConfig()
        {
            List<ConfigModel> models = new List<ConfigModel>();
            var data = GetAllConfigDataTable();
            if (data?.Rows.Count > 0)
            {
                foreach (DataRow row in data?.Rows)
                {
                    var id = row[0];
                    var isDefault = row[1];
                    var name = row[2];
                    var status = row[3];
                    var createdUid = row[4];
                    var createdDateUtc = row[5];
                    var updatedDateUtc = row[6];
                    var updatedUid = row[7];
                    models.Add(new ConfigModel()
                    {
                        Id = (string)id,
                        IsDefault = (bool)isDefault,
                        Name = (string)name,
                        Status = (short)status,
                        CreatedUid = (string)createdUid,
                        CreatedDateUtc = (DateTime?)createdDateUtc,
                        UpdatedDateUtc = (DateTime?)updatedDateUtc,
                        UpdatedUid = (string)updatedUid
                    });
                }
            }
            return models;
        }
        public bool Add(ConfigModel model)
        {
            var queryString = DaihuSqlCommand.Insertconfig(model);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        public bool Update(ConfigModel model)
        {
            var queryString = DaihuSqlCommand.UpdateConfig(model);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        public bool Delete(string id)
        {
            var queryString = DaihuSqlCommand.DeleteConfig(id);
            var result = ExecuteNonQuery(queryString);
            return result > 0;
        }
        private DataTable GetByIdDataTable(string id) => Gets(DaihuSqlCommand.GetConfigById(id));
        public ConfigModel GetById(string idParam)
        {
            var data = GetByIdDataTable(idParam);
            if (data?.Rows.Count > 0)
            {
                var row = data?.Rows[0];
                if (row != null)
                {
                    var id = row[0];
                    var isDefault = row[1];
                    var name = row[2];
                    var status = row[3];
                    var createdUid = row[4];
                    var createdDateUtc = row[5];
                    var updatedDateUtc = row[6];
                    var updatedUid = row[7];
                    return new ConfigModel()
                    {
                        Id = (string)id,
                        IsDefault = (bool)isDefault,
                        Name = (string)name,
                        Status = (short)status,
                        CreatedUid = (string)createdUid,
                        CreatedDateUtc = (DateTime?)createdDateUtc,
                        UpdatedDateUtc = (DateTime?)updatedDateUtc,
                        UpdatedUid = (string)updatedUid
                    };
                }
            }
            return null;
        }
    }
}
