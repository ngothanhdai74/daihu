﻿namespace VsExtension
{
    public class DaihuSqlCommand
    {
        #region Config
        public static string CreateTableConfig()
            => @"CREATE TABLE Config (
                    Id varchar(50) NOT NULL PRIMARY KEY,
	                IsDefault bit NOT NULL,
	                Name nvarchar(1024) NOT NULL,
	                Status smallint NOT NULL,
	                CreatedUid varchar(50) NULL,
	                CreatedDateUtc datetime NULL,
	                UpdatedDateUtc datetime NULL,
	                UpdatedUid varchar(50) NULL
                );";
        public static string GetAllConfig() => "select * from Config";
        public static string SearchConfig(string keyword) => $"SELECT * FROM Config WHERE Name LIKE '%{keyword}%';";
        public static string GetConfigById(string id) => $"select * from Config where Id ='{id}'";
        public static string DeleteConfig(string id) => $"DELETE FROM Config WHERE Id='{id}';";
        public static string UpdateConfig(ConfigModel model)
        => $"UPDATE Config SET Name = '{model.Name}',IsDefault={(model.IsDefault == true ? 1 : 0)}, Status= {model.Status},UpdatedDateUtc='{model.UpdatedDateUtc.ToString()}' WHERE Id = '{model.Id}';";
        public static string Insertconfig(ConfigModel model) =>
        $"INSERT INTO Config (Id, IsDefault, Name, Status, CreatedUid, CreatedDateUtc, UpdatedDateUtc, UpdatedUid) VALUES ('{model.Id}',{(model.IsDefault == true ? 1 : 0)},'{model.Name}',{model.Status},'{model.CreatedUid}','{model.CreatedDateUtc.ToString()}','{model.UpdatedDateUtc.ToString()}','{model.UpdatedUid}');";
        #endregion
        #region ConfigDetail
        public static string CreateTableConfigDetail()
            => @"CREATE TABLE ConfigDetail (
	                Id varchar(50) NOT NULL PRIMARY KEY,
	                ConfigId varchar(50) NOT NULL,
	                Status smallint NOT NULL,
	                FileName nvarchar(512) NOT NULL,
	                FolderPath nvarchar(1024) NOT NULL,
	                TextContent ntext NOT NULL,
	                CreatedUid varchar(50) NULL,
	                CreatedDateUtc datetime NULL,
	                UpdatedDateUtc datetime NULL,
	                UpdatedUid varchar(50) NULL
                );";
        public static string GetConfigDetailByConfigId(string configId) => $"select * from ConfigDetail where ConfigId = '{configId}'";
        public static string GetConfigDetailById(string id) => $"select * from ConfigDetail where Id = '{id}'";
        public static string SearchConfigDetailByFolderPath(string keyword) => $"SELECT * FROM ConfigDetail WHERE FolderPath LIKE '%{keyword}%';";
        public static string SearchConfigDetailByTextContent(string keyword) => $"SELECT * FROM ConfigDetail WHERE TextContent LIKE '%{keyword}%';";
        public static string InsertConfigDetail(ConfigDetailModel model) =>
        $"INSERT INTO ConfigDetail(Id,ConfigId,Status,FileName,FolderPath,TextContent,CreatedUid,CreatedDateUtc,UpdatedDateUtc,UpdatedUid) VALUES ('{model.Id}','{model.ConfigId}',{model.Status},'{model.FileName}','{model.FolderPath}','{model.TextContent}','{model.CreatedUid}','{model.CreatedDateUtc.ToString()}','{model.UpdatedDateUtc.ToString()}','{model.UpdatedUid}');";
        public static string DeleteConfigDetail(string id) => $"DELETE FROM ConfigDetail WHERE Id='{id}';";
        public static string DeleteConfigDetailByConfigId(string configId) => $"DELETE FROM ConfigDetail WHERE ConfigId='{configId}';";
        public static string UpdateConfigDetail(ConfigDetailModel model)
        => $"UPDATE ConfigDetail SET ConfigId = '{model.ConfigId}',FileName='{model.FileName}', Status= {model.Status},UpdatedDateUtc='{model.UpdatedDateUtc}',FolderPath='{model.FolderPath}',TextContent='{model.TextContent}' WHERE Id = '{model.Id}';";
        #endregion
    }
}
