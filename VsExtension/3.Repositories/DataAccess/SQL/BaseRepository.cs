﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class BaseRepository
    {
        private string SqlConnectionString = DaihuConnection.CanaviConnectionString;
        protected DataTable Gets(string sqlCommand)
        {
            SqlConnection connection = null;
            DataTable dt = new DataTable();
            try
            {
                connection = new SqlConnection(SqlConnectionString);
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                connection.Open();
                //----------------------------------------------------------------------------------
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("lỗi xảy ra:" + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return dt;
        }
        protected int ExecuteNonQuery(string sqlCommand)
        {
            SqlConnection connection = null;
            DataTable dt = new DataTable();
            try
            {
                connection = new SqlConnection(SqlConnectionString);
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                connection.Open();
                //----------------------------------------------------------------------------------
                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sqlCommand;
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("lỗi xảy ra:" + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return -1;
        }
    }
}
