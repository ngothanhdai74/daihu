﻿namespace VsExtension.Application.FileGeneration
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using VsExtension.Repositories.DataAccess;

    /// <summary>
    /// Interaction logic for FileGenerationControl.
    /// </summary>
    public partial class FileGenerationControl : UserControl
    {
        private readonly VsConfigAppService vsConfigAppService;
        private readonly VsConfigDetailAppService vsConfigDetailAppServices;
        private GenerationFileModel GenerationFileModel;
        private List<ConfigDetailModel> ConfigDetailSelected;

        public FileGenerationControl()
        {
            this.InitializeComponent();
            vsConfigAppService = new VsConfigAppService();
            vsConfigDetailAppServices = new VsConfigDetailAppService();
            GenerationFileModel = new GenerationFileModel();
        }
        private void MyToolWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var configs = vsConfigAppService.GetAllConfig();
            dropdownConfigs.ItemsSource = configs;
            dropdownConfigs.DisplayMemberPath = nameof(ConfigModel.Name);
            dropdownConfigs.SelectedValuePath = nameof(ConfigModel.Id);
            cmbDataTypes.ItemsSource = DataTypeModel.GetDataTypes;
            cmbDataTypes.DisplayMemberPath = nameof(DataTypeModel.DataTypeName);
            cmbDataTypes.SelectedValuePath = nameof(DataTypeModel.DataTypeValue);
            var defaultValue = configs.FirstOrDefault(m => m.IsDefault);
            if (defaultValue == null)
            {
                defaultValue = configs.FirstOrDefault();
            }
            dropdownConfigs.SelectedItem = defaultValue;
            var data = vsConfigDetailAppServices.GetConfigDetailByConfigId(defaultValue.Id);
            if (data?.Any() == true)
            {
                ConfigDetailSelected = data;
            }
        }
        private void dropdownConfigs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dropdownConfigs != null && dropdownConfigs.SelectedValue != null)
            {
                if (!string.IsNullOrWhiteSpace(dropdownConfigs.SelectedValue.ToString()))
                {
                    var data = vsConfigDetailAppServices.GetConfigDetailByConfigId(dropdownConfigs.SelectedValue.ToString());
                    if (data?.Any() == true)
                    {
                        ConfigDetailSelected = data;
                    }
                }
            }
        }
        private void btnSaveTableName_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txbTableName.Text))
            {
                GenerationFileModel.TableName = txbTableName.Text;
                lblTableName.Text = $"Tên Table: {txbTableName.Text}";
            }
            else
            {
                MessageBox.Show("Tên Table không được để trống");
            }
        }
        private void btnFieldName_Click(object sender, RoutedEventArgs e)
        {
            var datatype = cmbDataTypes.SelectedValue as string;
            if (string.IsNullOrWhiteSpace(datatype))
            {
                MessageBox.Show("Kiểu dữ liệu chưa được chọn");
            }
            else if (string.IsNullOrWhiteSpace(txbFieldName.Text))
            {
                MessageBox.Show("Tên Field không được để trống");
            }
            else
            {
                var field = new FieldModel()
                {
                    DataType = datatype,
                    FieldName = txbFieldName.Text
                };
                GenerationFileModel.Fields.Add(field);
                lvFields.Items.Add(field);
            }
        }
        private void btnGenerateFile_Click(object sender, RoutedEventArgs e)
        {
            if (GenerationFileModel.IsValidate())
            {
                var res = vsConfigAppService.GenerateFile(GenerationFileModel, ConfigDetailSelected);
                if (res)
                {
                    MessageBox.Show("Thành công");
                }
            }
            else
            {
                MessageBox.Show("Dữ liệu không hợp lệ");
            }
        }
    }
}