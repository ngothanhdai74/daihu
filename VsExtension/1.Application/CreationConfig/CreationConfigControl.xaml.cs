﻿namespace VsExtension.Application.CreationConfig
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    public partial class CreationConfigControl : UserControl
    {
        private readonly VsConfigAppService vsConfigAppService;
        private readonly VsConfigDetailAppService vsConfigDetailAppServices;
        private ConfigModel config;
        private List<ConfigDetailModel> configDetails;
        private int configDetailIndex;
        public CreationConfigControl()
        {
            this.InitializeComponent();
            vsConfigAppService = new VsConfigAppService();
            vsConfigDetailAppServices = new VsConfigDetailAppService();
        }
        private void MyToolWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var data = vsConfigAppService.GetAllConfig();
            if (data?.Any() == true)
            {
                dropdownConfigs.ItemsSource = data;
                dropdownConfigs.DisplayMemberPath = nameof(ConfigModel.Name);
                dropdownConfigs.SelectedValuePath = nameof(ConfigModel.Id);
                //config
                var defaultItem = data.FirstOrDefault(m => m.IsDefault);
                if (defaultItem == null)
                {
                    defaultItem = data.FirstOrDefault();
                }
                dropdownConfigs.SelectedItem = defaultItem;
                txbConfig.Text = defaultItem.Name;
                cbxIsDefault.IsChecked = defaultItem.IsDefault;
                //config detail
                configDetailIndex = 0;
                var currentModel = configDetails[configDetailIndex];
                txbFolderPath.Text = currentModel.FolderPath;
                txbTextContent.Text = currentModel.TextContent;
                txbFileName.Text = currentModel.FileName;
            }
        }
        private void dropdownConfigs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var configId = dropdownConfigs.SelectedValue as string;
            if (!string.IsNullOrWhiteSpace(configId))
            {
                config = vsConfigAppService.GetById(configId);
                if (config == null)
                    return;
                //config
                txbConfig.Text = config.Name;
                cbxIsDefault.IsChecked = config.IsDefault;
                // config detail
                configDetails = vsConfigDetailAppServices.GetConfigDetailByConfigId(configId);
                if (configDetails?.Any() != true)
                    return;
                configDetailIndex = 0;
                var currentModel = configDetails[configDetailIndex];
                txbFolderPath.Text = currentModel.FolderPath;
                txbTextContent.Text = currentModel.TextContent;
                txbFileName.Text = currentModel.FileName;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)// add config
        {
            var text = txbConfig.Text;
            if (!string.IsNullOrWhiteSpace(text))
            {
                var model = new ConfigModel(text, cbxIsDefault.IsChecked.GetValueOrDefault());
                var result = vsConfigAppService.Add(model);
                if (result)
                {
                    dropdownConfigs.ItemsSource = vsConfigAppService.GetAllConfig();
                    MessageBox.Show("Thành công");
                }
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)// delete config
        {
            var configId = dropdownConfigs.SelectedValue as string;
            if (!string.IsNullOrWhiteSpace(configId))
            {
                var result = vsConfigAppService.Delete(configId);
                configDetailIndex = 0;
                if (result)
                {
                    var configs = vsConfigAppService.GetAllConfig();
                    if (configs?.Any() == true)
                    {
                        dropdownConfigs.ItemsSource = configs;
                        //config

                        config = configs.FirstOrDefault();
                        txbConfig.Text = config.Name;
                        cbxIsDefault.IsChecked = config.IsDefault;

                        // config detail
                        configId = config.Id;

                        configDetails = vsConfigDetailAppServices.GetConfigDetailByConfigId(configId);
                        if (configDetails?.Any() != true)
                            return;
                        var currentModel = configDetails[configDetailIndex];
                        txbFolderPath.Text = currentModel.FolderPath;
                        txbTextContent.Text = currentModel.TextContent;
                        txbFileName.Text = currentModel.FileName;
                    }
                    else
                    {
                        config = null;
                        configDetails = null;
                        cbxIsDefault.IsChecked = false;
                        txbConfig.Text = string.Empty;
                        txbFolderPath.Text = string.Empty;
                        txbTextContent.Text = string.Empty;
                        txbFileName.Text = string.Empty;
                    }
                }
            }
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)// update config
        {
            var text = txbConfig.Text;
            if (!string.IsNullOrWhiteSpace(text) && config != null)
            {
                config.Name = text;
                config.IsDefault = cbxIsDefault.IsChecked.GetValueOrDefault();
                var res = vsConfigAppService.Update(config);
                if (res)
                {
                    dropdownConfigs.ItemsSource = vsConfigAppService.GetAllConfig();
                    MessageBox.Show("Thành công");
                }
            }
        }
        private void Button_Click_3(object sender, RoutedEventArgs e)// add config detail
        {
            if (!string.IsNullOrWhiteSpace(txbFolderPath.Text) && !string.IsNullOrWhiteSpace(txbTextContent.Text))
            {
                var model = new ConfigDetailModel(config.Id, txbFolderPath.Text, txbTextContent.Text, txbFileName.Text);
                var res = vsConfigDetailAppServices.Insert(model);
                if (res)
                {
                    configDetails.Add(model);
                    MessageBox.Show("Thành công");
                }
            }
            else
            {
                MessageBox.Show("Không được để trống");
            }
        }
        private void Button_Click_4(object sender, RoutedEventArgs e)// delete config detail
        {
            if (configDetails?.Any() == true)
            {
                var currentModel = configDetails[configDetailIndex];
                if (currentModel != null)
                {
                    var res = vsConfigDetailAppServices.DeleteById(currentModel.Id);
                    if (res)
                    {
                        if (configDetailIndex > 0)
                        {
                            configDetailIndex--;
                            currentModel = configDetails[configDetailIndex];
                            txbFolderPath.Text = currentModel.FolderPath;
                            txbTextContent.Text = currentModel.TextContent;
                            txbFileName.Text = currentModel.FileName;
                        }
                        else
                        {
                            txbFolderPath.Text = string.Empty;
                            txbTextContent.Text = string.Empty;
                            txbFileName.Text = string.Empty;
                        }
                        MessageBox.Show("Thành công");
                    }
                }
            }
        }
        private void Button_Click_5(object sender, RoutedEventArgs e)// update config detail
        {
            if (configDetails?.Any() == true)
            {
                var currentModel = configDetails[configDetailIndex];
                if (currentModel != null)
                {
                    currentModel.TextContent = txbTextContent.Text;
                    currentModel.FolderPath = txbFolderPath.Text;
                    currentModel.FileName = txbFileName.Text;
                    var res = vsConfigDetailAppServices.Update(currentModel);
                    if (res)
                    {
                        MessageBox.Show("Thành công");
                    }
                }
            }
        }
        private void Button_Click_6(object sender, RoutedEventArgs e)// pre
        {
            if (configDetails?.Any() == true)
            {
                if (configDetailIndex <= 0)
                {
                    MessageBox.Show("Nhỏ hết cỡ rồi");
                }
                else
                {
                    configDetailIndex--;
                    var currentModel = configDetails[configDetailIndex];
                    txbFolderPath.Text = currentModel.FolderPath;
                    txbTextContent.Text = currentModel.TextContent;
                    txbFileName.Text = currentModel.FileName;
                }
            }
        }
        private void Button_Click_7(object sender, RoutedEventArgs e)// next
        {
            if (configDetails?.Any() == true)
            {
                if (configDetailIndex >= configDetails.Count - 1)
                {
                    MessageBox.Show("Lớn hết cỡ rồi");
                }
                else
                {
                    configDetailIndex++;
                    var currentModel = configDetails[configDetailIndex];
                    txbFolderPath.Text = currentModel.FolderPath;
                    txbTextContent.Text = currentModel.TextContent;
                    txbFileName.Text = currentModel.FileName;
                }
            }

        }
    }
}