﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;

namespace VsExtension.Application.CreationConfig
{
    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    /// <para>
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </para>
    /// </remarks>
    [Guid("057a4b9d-3f6c-4e17-9adc-40112d592a60")]
    public class CreationConfig : ToolWindowPane
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreationConfig"/> class.
        /// </summary>
        public CreationConfig() : base(null)
        {
            this.Caption = "CreationConfig";

            // This is the user control hosted by the tool window; Note that, even if this class implements IDisposable,
            // we are not calling Dispose on this object. This is because ToolWindowPane calls Dispose on
            // the object returned by the Content property.
            this.Content = new CreationConfigControl();
        }
    }
}
