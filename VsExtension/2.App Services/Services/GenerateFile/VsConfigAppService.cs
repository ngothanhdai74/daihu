﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VsExtension.Common.Files;
using VsExtension.Repositories.DataAccess;

namespace VsExtension
{
    public class VsConfigAppService
    {
        private readonly VsConfigRepository _vsConfigRepository;
        private readonly VsConfigDetailRepository _vsConfigDetailRepository;
        public VsConfigAppService()
        {
            _vsConfigRepository = new VsConfigRepository();
            _vsConfigDetailRepository = new VsConfigDetailRepository();
        }
        public List<ConfigModel> GetAllConfig() => _vsConfigRepository.GetAllConfig();
        public bool Add(ConfigModel model) => _vsConfigRepository.Add(model);
        public bool Delete(string id)
        {
            var result = _vsConfigRepository.Delete(id);
            if (result)
            {
                _vsConfigDetailRepository.Delete(id);
                return result;
            }
            return false;
        }
        public ConfigModel GetById(string id) => _vsConfigRepository.GetById(id);
        public bool Update(ConfigModel model) => _vsConfigRepository.Update(model);
        public bool GenerateFile(GenerationFileModel generationFile, List<ConfigDetailModel> configDetails)
        {
            try
            {
                var keyword = generationFile.TableName;
                foreach (var configDetail in configDetails)
                {
                    //hander text content
                    var textContent = configDetail.TextContent.Replace(DaihuConstant.TableNameRegex, keyword);
                    if (textContent.Contains(DaihuConstant.FieldsNameRegex))
                    {
                        var fieldValue = HandlerFields(textContent, generationFile.Fields);
                        textContent = textContent.Replace(DaihuConstant.FieldsNameRegex, fieldValue);
                    }
                    // handler file name
                    var fileName = configDetail.FileName.Replace(DaihuConstant.TableNameRegex, keyword);
                    // create folder if not exist
                    FolderFunctionHelper.CreateFolder(configDetail.FolderPath, keyword);
                    // create file
                    var absoluteFile = FileFunctionHelper.CombineFile(configDetail.FolderPath, keyword, fileName);
                    if (!FileFunctionHelper.IsFileExists(absoluteFile))
                    {
                        FileFunctionHelper.CreateFile(absoluteFile, textContent);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private string HandlerFields(string textContent, List<FieldModel> Fields)
        {

            var result = "";
            foreach (var field in Fields)
            {
                result += $"public {field.DataType} {field.FieldName} {{ get; set; }}\n\t\t";
            }
            return result;
        }
    }
}
