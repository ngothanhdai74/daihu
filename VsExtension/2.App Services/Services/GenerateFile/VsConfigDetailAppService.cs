﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class VsConfigDetailAppService
    {
        private VsConfigDetailRepository _vsConfigDetailRepository;
        public VsConfigDetailAppService()
        {
            _vsConfigDetailRepository = new VsConfigDetailRepository();
        }
        public List<ConfigDetailModel> GetConfigDetailByConfigId(string configIdParam)
            => _vsConfigDetailRepository.GetConfigDetailByConfigId(configIdParam);
        public bool Delete(string configId) => _vsConfigDetailRepository.Delete(configId);
        public bool DeleteById(string id) => _vsConfigDetailRepository.DeleteById(id);
        public bool Insert(ConfigDetailModel model) => _vsConfigDetailRepository.Insert(model);
        public bool Update(ConfigDetailModel model) => _vsConfigDetailRepository.Update(model);
    }
}
