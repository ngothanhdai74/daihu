﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension.Common.Files
{
    public static class FileFunctionHelper
    {
        public static string CombineFile(string folderPath, string folderName, string fileName) => Path.Combine(folderPath, folderName, fileName);
        public static void CreateFile(string fullPathFileName, string contentFile) => File.WriteAllText(fullPathFileName, contentFile);
        public static bool IsFileExists(string pathFile) => File.Exists(pathFile);
        public static void Delete(string pathFile) => File.Delete(pathFile);
        public static void CopyFile(string source, string detination) => File.Copy(source, detination);
        public static void MoveFile(string source, string detination) => File.Move(source, detination);
    }
}
