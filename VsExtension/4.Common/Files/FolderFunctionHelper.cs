﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension.Common.Files
{
    public static class FolderFunctionHelper
    {
        public static void CreateFolder(string folderPath, string folderName)
        {
            var directory = Path.Combine(folderPath, folderName);
            if (!Directory.Exists(directory))
            {
                var data = Directory.CreateDirectory(directory);
            }
        }
        public static bool DeleteFolder(string folderPath, string folderName)
        {
            var directory = Path.Combine(folderPath, folderName);
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory);
            }
            return false;
        }
        public static bool MoveFolder(string sourceDirName, string destDirName)
        {
            try
            {
                Directory.Move(sourceDirName, destDirName);
                return true;
            }
            catch (IOException exp)
            {
                Console.WriteLine(exp.Message);
            }
            return false;
        }
    }
}
