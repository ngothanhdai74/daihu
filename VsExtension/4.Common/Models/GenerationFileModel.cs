﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class GenerationFileModel
    {
        public GenerationFileModel()
        {
            Fields = new List<FieldModel>();
        }
        public string TableName { get; set; }
        public List<FieldModel> Fields { get; set; }
        public bool IsValidate() => (!string.IsNullOrWhiteSpace(TableName) && Fields.Any());
    }
    public class FieldModel
    {
        public string FieldName { get; set; }
        public string DataType { get; set; }
    }
}
