﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class BaseModel
    {
        public BaseModel()
        {

        }
        public BaseModel(string id, short status, string createdUid, DateTime? createdDateUtc, DateTime? updatedDateUtc, string updatedUid)
        {
            Id = id;
            Status = status;
            CreatedUid = createdUid;
            CreatedDateUtc = createdDateUtc;
            UpdatedDateUtc = updatedDateUtc;
            UpdatedUid = updatedUid;
        }
        public string Id { get; set; }
        public short Status { get; set; }
        public string CreatedUid { get; set; }
        public DateTime? CreatedDateUtc { get; set; }
        public DateTime? UpdatedDateUtc { get; set; }
        public string UpdatedUid { get; set; }
    }
}
