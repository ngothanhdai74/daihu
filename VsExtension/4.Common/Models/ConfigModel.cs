﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class ConfigModel : BaseModel
    {
        public ConfigModel() : base(Guid.NewGuid().ToString("N"), 1, string.Empty, DateTime.UtcNow, DateTime.UtcNow, string.Empty)
        {

        }
        public ConfigModel(string name, bool isDefault) : base(Guid.NewGuid().ToString("N"), 1, string.Empty, DateTime.UtcNow, DateTime.UtcNow, string.Empty)
        {
            Name = name;
            IsDefault = isDefault;
        }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
}
