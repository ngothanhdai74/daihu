﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class DataTypeModel
    {
        public string DataTypeName { get; set; }
        public string DataTypeValue { get; set; }
        public static List<DataTypeModel> GetDataTypes
        {
            get
            {
                return new List<DataTypeModel>() {
                    new DataTypeModel()
                    {
                        DataTypeName = "long",
                        DataTypeValue = "long"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "int",
                        DataTypeValue = "int"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "double",
                        DataTypeValue = "double"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "string",
                        DataTypeValue = "string"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "Datetime",
                        DataTypeValue = "Datetime"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "bool",
                        DataTypeValue = "bool"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "long?",
                        DataTypeValue = "long?"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "int?",
                        DataTypeValue = "int?"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "double?",
                        DataTypeValue = "double?"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "string?",
                        DataTypeValue = "string?"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "Datetime?",
                        DataTypeValue = "Datetime?"
                    },
                    new DataTypeModel()
                    {
                        DataTypeName = "bool?",
                        DataTypeValue = "bool?"
                    },
                };
            }
        }
    }
}
