﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension
{
    public class ConfigDetailModel : BaseModel
    {
        public ConfigDetailModel()
        {

        }
        public ConfigDetailModel(string configId, string folderPath, string textcontent, string fileName) : base(Guid.NewGuid().ToString("N"), 1, string.Empty, DateTime.UtcNow, DateTime.UtcNow, string.Empty)
        {
            FileName = fileName;
            ConfigId = configId;
            FolderPath = folderPath;
            TextContent = textcontent;
        }
        public string ConfigId { get; set; }
        public string FileName { get; set; }
        public string FolderPath { get; set; }
        public string TextContent { get; set; }
    }
}
