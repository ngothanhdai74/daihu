﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace VsExtension.Common.Http
{
    public class HttpClientFunctionHelper
    {
        public static async Task<TResponse> GetAsync<TResponse>(string url)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                var data = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TResponse>(data);
            }
        }
        public static async Task<TResponse> PostAsync<TResponse, TRequest>(string url, TRequest model)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, data);
                response.EnsureSuccessStatusCode();
                var res = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TResponse>(res);
            }
        }
    }
}
