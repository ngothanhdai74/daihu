﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Daihu
{
    public static class Serialization
    {
        public static string JsonSerialize<T>(T obj) => JsonSerializer.Serialize<T>(obj);
        public static T JsonDeseralize<T>(string json) => JsonSerializer.Deserialize<T>(json);
        public static byte[] ProtoBufSerialize(object item)
        {
            using(var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, item);
                return ms.ToArray();
            }
        }
        public static T ProtoBufDeserialize<T>(byte[] byteArray)
        {
            using (var ms = new MemoryStream(byteArray))
            {
                return Serializer.Deserialize<T>(ms);
            }
        }
    }
}
