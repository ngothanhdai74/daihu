﻿using NETCore.Encrypt;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Daihu
{
    public static class Encryptor
    {
        public static void CreateKey()
        {
            var rsaKey = EncryptProvider.CreateRsaKey();
            var absoluteFile = FileFunctionHelper.CombineFile(@"D:\secret_at_git", "key", "PrivateKey.sc");
            if (!FileFunctionHelper.IsFileExists(absoluteFile))
            {
                FileFunctionHelper.CreateFile(absoluteFile, rsaKey.PrivateKey);
            }
            absoluteFile = FileFunctionHelper.CombineFile(@"D:\secret_at_git", "key", "PublicKey.sc");
            if (!FileFunctionHelper.IsFileExists(absoluteFile))
            {
                FileFunctionHelper.CreateFile(absoluteFile, rsaKey.PublicKey);
            }
        }
        public static string GetPublicKey()
        {
            try
            {
                var absoluteFile = FileFunctionHelper.CombineFile(@"D:\secret_at_git", "key", "PublicKey.sc");
                // Open the text file using a stream reader.
                using (var sr = new StreamReader(absoluteFile))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return string.Empty;
        }
        public static string GetPrivateKey()
        {
            try
            {
                var absoluteFile = FileFunctionHelper.CombineFile(@"D:\secret_at_git", "key", "PrivateKey.sc");
                using (var sr = new StreamReader(absoluteFile))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return string.Empty;
        }
    }
}
