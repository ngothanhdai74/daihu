﻿using Humanizer;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Daihu
{
    public static class Extensions
    {
        private readonly static string TimeZoneVnId = "SE Asia Standard Time";
        public static string DisplayDateTime(this DateTime dateTime)
        {
            return dateTime.Humanize(true);
        }
        public static string AsMoney(this decimal item, bool haveCurrency = true)
        {
            if (item > 0)
            {
                if (haveCurrency)
                {
                    return item.ToString("#,##.## đ", new CultureInfo("vi-VN"));
                }
                else
                {
                    return item.ToString("#,##.##", new CultureInfo("vi-VN"));
                }
            }
            if (haveCurrency)
            {
                return string.Format("0 đ");
            }
            else
            {
                return string.Format("0");
            }

        }
        public static string AsMoney(this int item, bool haveCurrency = true)
        {
            if (item > 0)
            {
                if (haveCurrency)
                {
                    return item.ToString("#,##.## đ", new CultureInfo("vi-VN"));
                }
                else
                {
                    return item.ToString("#,##.##", new CultureInfo("vi-VN"));
                }
            }
            if (haveCurrency)
            {
                return string.Format("0 đ");
            }
            else
            {
                return string.Format("0");
            }
        }
        public static string AsMoney(this decimal? item, bool haveCurrency = true)
        {
            if (item.HasValue)
            {
                if (haveCurrency)
                {
                    return item.Value.ToString("#,##.## đ", new CultureInfo("vi-VN"));
                }
                else
                {
                    return item.Value.ToString("#,##.##", new CultureInfo("vi-VN"));
                }
            }
            if (haveCurrency)
            {
                return string.Format("0 đ");
            }
            else
            {
                return string.Format("0");
            }
        }
        public static string AsMoney(this int? item, bool haveCurrency = true)
        {
            if (item.HasValue)
            {
                if (haveCurrency)
                {
                    return item.Value.ToString("#,##.## đ", new CultureInfo("vi-VN"));
                }
                else
                {
                    return item.Value.ToString("#,##.##", new CultureInfo("vi-VN"));
                }
            }
            if (haveCurrency)
            {
                return string.Format("0 đ");
            }
            else
            {
                return string.Format("0");
            }
        }
        public static string AsDateView(this DateTime item)
        {
            return item.ToString("dd/MM/yyyy");
        }
        public static string AsDateTimeView(this DateTime item)
        {
            return item.ToString("dd/MM/yyyy HH:mm");
        }
        public static DateTime ToVietNamDateTimeFromUtc(this DateTime item)
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneVnId);
            return TimeZoneInfo.ConvertTimeFromUtc(item, cstZone);
        }
        public static DateTime ToUtcFromVietNamDateTime(this DateTime item)
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneVnId);
            return TimeZoneInfo.ConvertTimeToUtc(item, cstZone);
        }
        public static DateTime ConvertDateTimeFromUtc(this DateTime item, string systemTimeZoneId)
        {
            var tzis = TimeZoneInfo.GetSystemTimeZones();
            if (tzis.Any(m => m.StandardName.Equals(systemTimeZoneId)))
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(systemTimeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(item, cstZone);
            }
            throw new Exception($"{systemTimeZoneId} is not found");
        }
        public static DateTime ConvertDateTimeToUtc(this DateTime item, string systemTimeZoneId)
        {
            var tzis = TimeZoneInfo.GetSystemTimeZones();
            if (tzis.Any(m => m.StandardName.Equals(systemTimeZoneId)))
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(systemTimeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(item, cstZone);
            }
            throw new Exception($"{systemTimeZoneId} is not found");
        }
        public static string ToSql<TEntity>(this IQueryable<TEntity> query) where TEntity : class
        {
            var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.Private("_relationalCommandCache");
            var selectExpression = relationalCommandCache.Private<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.Private<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");

            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);

            string sql = command.CommandText;
            return sql;
        }
        private static object Private(this object obj, string privateField) => obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);
        private static T Private<T>(this object obj, string privateField) => (T)obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);
        public static void SetPropValue(this object src, string propName, object value)
        {
            src.GetType().GetProperty(propName).SetValue(src, value);
        }
    }
}
