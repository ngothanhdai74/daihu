﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Daihu
{
    public static class Function
    {

        public static bool IsValidEmail(string email)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            var regex = new Regex(pattern, RegexOptions.IgnoreCase);

            return regex.IsMatch(email);
        }
        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            string pattern = @"(09|01[2|6|8|9]|03|02[1|2|3|4|5|6|7|8|9]|08)+([0-9]{8})\b|^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";

            var regex = new Regex(pattern, RegexOptions.IgnoreCase);

            return regex.IsMatch(phoneNumber);
        }
        public static string GetMessage(this Exception exception)
        {
            if (exception != null) return $"Message: {exception.Message},Data: {Serialization.JsonSerialize(exception.Data)},StackTrace: {exception.StackTrace}";
            return string.Empty;
        }
    }
}
