﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
namespace Daihu
{
    public static class Utility
    {
        public static IConfigurationRoot GetSystemConfigDotnetCore()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory());
            configurationBuilder.AddJsonFile("appsettings.json", false);
            configurationBuilder.AddJsonFile($"appsettings.{environmentName}.json", true, true);
            return configurationBuilder.Build();
        }
    }
}
