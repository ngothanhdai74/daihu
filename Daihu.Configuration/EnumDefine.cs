﻿using System.ComponentModel;

namespace Daihu.Configuration
{
    public class EnumDefine
    {
        public enum UserStatus
        {
            [Description("Đang hoạt động")]
            Active = 1,
            [Description("Không hoạt động")]
            InActive = 2,
            [Description("Mới tạo")]
            New = 3,
            [Description("Đang bị khóa")]
            LockOut = 4,
            [Description("Đã xóa")]
            Delete = 5
        }
    }
}
