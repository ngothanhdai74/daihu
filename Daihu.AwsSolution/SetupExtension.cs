﻿using Amazon.S3;
using Daihu.AwsSolution.S3.Implements;
using Daihu.AwsSolution.S3.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Daihu
{
    public static class SetupExtension
    {
        public static void AwsS3(this IServiceCollection services)
        {
            services.AddAWSService<IAmazonS3>();
            services.AddTransient<IDaihuAwsS3Service, DaihuAwsS3Service>();
        }
    }
}
