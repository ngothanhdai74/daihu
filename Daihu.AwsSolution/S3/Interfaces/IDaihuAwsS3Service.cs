﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.AwsSolution.S3.Interfaces
{
    public interface IDaihuAwsS3Service
    {
        Task<int> CreateFolder(string bucketName, string newFolderName, string prefix = "");
    }
}
