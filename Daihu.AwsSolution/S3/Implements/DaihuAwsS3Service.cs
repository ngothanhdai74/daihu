﻿using Amazon.S3;
using Amazon.S3.Model;
using Daihu.AwsSolution.S3.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Daihu.AwsSolution.S3.Implements
{
    public class DaihuAwsS3Service : IDaihuAwsS3Service
    {
        private readonly IAmazonS3 _amazonS3;
        public DaihuAwsS3Service(IAmazonS3 amazonS3)
        {
            _amazonS3 = amazonS3;
        }
        public async Task<int> CreateFolder(string bucketName, string newFolderName, string prefix = "")
        {
            var request = new PutObjectRequest();

            request.BucketName = bucketName;

            request.Key = $"{prefix.Replace("/", string.Empty).Trim()}/{newFolderName.Replace("/", string.Empty).Trim()}/";

            var response = await _amazonS3.PutObjectAsync(request);

            return (int)response.HttpStatusCode;
        }
        public async Task<string> UploadFile(string bucketName, string newFolderName, string prefix = "")
        {
            return string.Empty;
        }
    }
}
