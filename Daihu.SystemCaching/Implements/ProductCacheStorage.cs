﻿using Daihu.Caching;
using Daihu.Caching.Interfaces;
using Daihu.SystemCaching.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Daihu.SystemCaching.Implements
{
    public class ProductCacheStorage : IProductCacheStorage
    {
        private readonly IRedisStorage _redisStorage;
        public ProductCacheStorage(IRedisStorage redisStorage)
        {
            _redisStorage = redisStorage;
        }
    }
}
