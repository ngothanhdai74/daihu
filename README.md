# Tutorial : Setup project Architecture Microservice .Net Core


1. Tạo mới project template
- `dotnet new is4aspid -n Davi.SSO`
- Bỏ dòng app.UseDatabaseErrorPage();
2.add nuget package
- `IdentityServer4.EntityFramework`
- `Microsoft.EntityFrameworkCore.SqlServer`

3.Chỉnh sửa file Startup trong SSO

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            string connectionString = Configuration.GetConnectionString("ConfigSSOConnection");

            var builder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                })
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddAspNetIdentity<ApplicationUser>();
                
  4.Adding Migrations
  -`dotnet tool install --global dotnet-ef`
  
-NHỚ COMMIT LÊN

 - download package `Microsoft.EntityFrameworkCore.Design`
 - `dotnet ef migrations add InitialIdentityServerPersistedGrantDbMigration -c PersistedGrantDbContext -o Data/Migrations/IdentityServer/PersistedGrantDb`
 - `dotnet ef migrations add InitialIdentityServerConfigurationDbMigration -c ConfigurationDbContext -o Data/Migrations/IdentityServer/ConfigurationDb`
 
 5.Initializing the Database
 
 
    private void InitializeDatabase(IApplicationBuilder app)
      {
      using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

        var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
        context.Database.Migrate();
        if (!context.Clients.Any())
        {
            foreach (var client in Config.Clients)
            {
                context.Clients.Add(client.ToEntity());
            }
            context.SaveChanges();
        }

        if (!context.IdentityResources.Any())
        {
            foreach (var resource in Config.Ids)
            {
                context.IdentityResources.Add(resource.ToEntity());
            }
            context.SaveChanges();
        }

        if (!context.ApiResources.Any())
        {
            foreach (var resource in Config.Apis)
            {
                context.ApiResources.Add(resource.ToEntity());
            }
            context.SaveChanges();
        }
      }
    }
6. Chaỵ Function init database

-`InitializeDatabase(app);`

- Cách Tạo CERTIFICATE X509 CHO SSO 


1.   [link tham khảo cách get certificates X509](https://docs.microsoft.com/vi-vn/archive/blogs/kaevans/using-powershell-with-certificates)
5.   vào project của SSO. bật powelShell lên
6.   copy đoạn này vào powelShell
7.   `$cert = New-SelfSignedCertificate -Subject "CN=DaviCert" -CertStoreLocation cert:\CurrentUser\My -Provider "Microsoft Strong Cryptographic Provider"`
8.   chạy đoạn bên dưới để kiểm tra cert đã tồn tại hay chưa
9.   `Get-ChildItem -Path cert:\CurrentUser\My`
10.   để remove 1 cái certificate thì chạy đoạn dưới(đổi $cert.Thumbprint bằng Id của certificate)
11.   `Remove-Item -Path ("cert:\CurrentUser\My\" + $cert.Thumbprint)`
12.   ví dụ nhé:
13.   `Remove-Item -Path ("cert:\CurrentUser\My\D8E3C50E130F1A7079EEC476EFA8363A7F5FC0E0")`
14.   bây giờ $cert là biến trong powelShell rồi. paste nó ra. nó sẽ ra được cert vừa tạo
15.   để export ra được cái file private key.
16.   đầu tiên:
17.   `$cred = Get-Credential`
18.   nhập xong sẽ phải nhập tài khoản mật khẩu tùy ý(nhớ để điện vào trong file startUp SSO)
19.   paste `$cred` để kiểm tra key vừa tạo
20.   cuối cùng là export file key ra
21.   `Export-PfxCertificate -Cert $cert -Password $cred.Password -FilePath "./is_cert.pfx"`
22.   kiểm tra project SSO xem có file **is_cert.pfx**
23.   tiếp theo : thêm file name fileName và password vào **X509Certificate2()**
24.   sau đó thêm 3 dòng code sau 
25.   `var filePath = Path.Combine(Environment.ContentRootPath, "is_cert.pfx");`
26.   `var certificate = new X509Certificate2(filePath,"Davidkmhd!1");`
27.   `builder.AddSigningCredential(certificate);`

--------------------Tóm tắt nhanh gọn -------------------------

1. `$cert = New-SelfSignedCertificate -Subject "CN=DaviCert" -CertStoreLocation cert:\CurrentUser\My -Provider "Microsoft Strong Cryptographic Provider"`
2.  `$cred = Get-Credential` (chọn :(account:davi|password:Davidkmhd!1))
3.  `Export-PfxCertificate -Cert $cert -Password $cred.Password -FilePath "./is_cert.pfx"`
4.  thêm dòng sau


     - `var filePath = Path.Combine(Environment.ContentRootPath, "is_cert.pfx");`
     - `var certificate = new X509Certificate2(filePath,"Davidkmhd!1");`
     - ` builder.AddSigningCredential(certificate);`
       
       
       
---------------------------------------------------------
*  cách generate Model cho aspnet.net idenity của con SSO : 
`dotnet ef database update --context ApplicationDbContext`
--------------------------------------------------------
[link tham khảo TEDU](https://tedu.com.vn/kien-thuc/cai-dat-va-su-dung-aspnet-identity-core-va-identity-server-4-trong-ung-dung-aspnet-core-270.html)

--------------------------------------------------------
